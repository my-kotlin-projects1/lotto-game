import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

suspend fun main() {
    val players = List(2){ Player(2) }
    players.forEach { it.printCards() }
    val gameScope = CoroutineScope(Job() + Dispatchers.Unconfined)

    val mainJob = gameScope.launch {
        players.forEach { player ->
            val playerNumber = players.indexOf(player) + 1
            player.cards.forEach { card ->
                var counter = 0
                val cardNumber = player.cards.indexOf(card) + 1
                val jobs = MutableList<Job?>(0){ null }
                val job = launch {
                    GeneratorShared.sharedFlow.collect { collectedNum ->
                        for (line in card) {
                            if (collectedNum in line) {
                                println("совпало $collectedNum")
                                counter += 1
                                println("counter of card $cardNumber of player $playerNumber = $counter")
                                if (counter == 15) {
                                    println("!!!!Player $playerNumber WINS with card № $cardNumber !!!")
                                    GeneratorShared.scope.cancel()
                                    gameScope.cancel()
                                    jobs.forEach { it?.cancel() }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    mainJob.join()
}

