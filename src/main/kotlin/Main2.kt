import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlin.random.Random

fun main4() {
    val player1 = Player(2)
    player1.printCards()

    runBlocking {
        player1.cards.forEach { card ->
            var counter = 0
            val cardNumber = player1.cards.indexOf(card) + 1
            launch {
                GeneratorShared.sharedFlow.collect() { collectedNum ->
                    for (line in card) {
                        if (collectedNum in line) {
                            println("совпало $collectedNum")
                            counter += 1
                            println("counter of card $cardNumber - $counter")
                            if (counter == 15) {
                                cancel()
                                println("!!!!card № $cardNumber of player1 WINS!!!")
                            }
                        }
                    }
                }
            }
        }


        /*launch {
            GeneratorShared.sharedFlow.collect()
            { println("Listener2 collected $it") }
        }*/
    }
}

object GeneratorShared2 {
    private val numbers = List(90) { it + 1 }.shuffled()
    private val scope = CoroutineScope(Job() + Dispatchers.Default)
    private val _sharedFlow = MutableSharedFlow<Int>()
    val sharedFlow = _sharedFlow.asSharedFlow()

    init {
        scope.launch {
            for (number in numbers) {
                _sharedFlow.emit(number)
            }
            delay(500)
        }
    }
}